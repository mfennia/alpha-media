<?php

/**
 * Implements hook_views_data().
 */
function custom_article_views_data() {
  $data['views']['text_custom_custom'] = [
    'title' => t('text_custom_custom'),
    'help' => t('text_custom_custom.'),
    'area' => [
      'id' => 'text_custom_custom',
    ],
  ];
  $data['views']['customarea'] = [
    'title' => t('customarea'),
    'help' => t('customarea.'),
    'area' => [
      'id' => 'customarea',
    ],
  ];

  return $data;
}
