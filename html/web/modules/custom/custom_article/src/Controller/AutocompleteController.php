<?php
namespace Drupal\custom_article\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\taxonomy\Entity\Term;

class AutocompleteController extends ControllerBase {

  public function handleAutocomplete(Request $request) {
    $matches = [];
    $typed_string = $request->query->get('q');

    // Remplacez 'intervenants' par le nom machine de votre vocabulaire de taxonomie.
    $vocabulaire = 'intervenants';

    $query = \Drupal::entityQuery('taxonomy_term');
    $query->accessCheck(True);
    $query->condition('vid', $vocabulaire);
    $query->condition('name', '%' . $typed_string . '%', 'LIKE');
    $query->range(0, 10); // Limitez le nombre de résultats si nécessaire.
    $term_ids = $query->execute();

    foreach ($term_ids as $term_id) {
      $term = Term::load($term_id);
      $matches[] = ['value' => $term->getName(), 'label' => $term->getName()];
    }

    return new JsonResponse($matches);
  }
}
