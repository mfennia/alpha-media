<?php

namespace Drupal\custom_article\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;

class CustomFrontPageController extends ControllerBase {
  public function customFrontPage() {
    // Load the latest article.
    $query = \Drupal::entityQuery('node')
      ->accessCheck(TRUE)
      ->condition('type', 'article')
->condition('field_mise_en_page_d_accueil', '1')
      ->sort('created', 'DESC')
      ->range(0, 1);
    $nids = $query->execute();
    $latest_article = reset($nids);

    // Redirect to the latest article if found.
    if ($latest_article) {
      $response = new RedirectResponse(\Drupal\Core\Url::fromUri('base:node/'.$latest_article)->toString());
      return $response; ;
    }
    // No articles found, you can handle this case as needed.
    return [
      '#markup' => $this->t('No articles found.'),
    ];
  }
public function iFrame() {
   $iframe = '<iframe src="https://alphaomegafondation.us20.list-manage.com/subscribe?u=5d262d63a3d61ffd3f6717b91&id=808c98fd83" width="100%" height="850px" style="border: none;"></iframe>';
    $response = new AjaxResponse();
    $options = [
      'width' => '800',
    ];
    $response->addCommand(new OpenModalDialogCommand('Subscribe', $iframe, $options));
    
    return $response;
  }
}
