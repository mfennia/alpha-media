<?php

namespace Drupal\custom_article\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;


class Intervenats extends FormBase {
  /**
   * {@inheritdoc}
   */
  // add function getformid
  public function getFormId() {
    return 'custom_article_intervenat';
  }
  public function buildForm(array $form, FormStateInterface $form_state) {
    $request = \Drupal::request();
    $defaultValue = $request->query->get('field_intervenants_target_id');
    $form['intervenant'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Nos intervenants'),
      '#autocomplete_route_name' => 'custom_article.autocomplete',
      '#default_value' => $defaultValue ?: '',
      '#attributes'    => [
        'autocompleteclose' => 'this.form.submit();',
      ],
    ];

    // Ajoutez le reste de la configuration de votre formulaire ici.
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Envoyer'),
      '#attributes' => [
        'style' => ['display: none;'],
      ],
    ];
    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $termId = $form_state->getValue('intervenant');

    //$texteModifie = str_replace(" ", "+", $termId);
    // Construire l'URL de redirection
    $url = \Drupal\Core\Url::fromUri('internal:/article-education-scolaire', [
      'query' => ['field_intervenants_target_id' => $termId]
    ]);

    // Rediriger vers l'URL
    $form_state->setRedirectUrl($url);
  }

}
