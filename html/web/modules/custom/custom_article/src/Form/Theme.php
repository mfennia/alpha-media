<?php

namespace Drupal\custom_article\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;


class Theme extends FormBase {
  /**
   * {@inheritdoc}
   */
  // add function getformid
  public function getFormId() {
    return 'custom_article_theme';
  }
  public function buildForm(array $form, FormStateInterface $form_state) {
    $terms =\Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('theme');
    $options = [];
    $options['All'] = '- Tous -';
    foreach ($terms as $term) {
      $options[$term->tid] = $term->name;
    }
    // ajouter un tire au formulaire

    $request = \Drupal::request();
    $defaultValue = $request->query->get('field_theme_target_id');
    $form['theme'] = [
      '#type' => 'select',
      '#title' => $this->t('Nos articles par thème'),
      '#options' => $options,
      //'#multiple' => TRUE,
      '#default_value' => $defaultValue ?: '',
      '#attributes'    => [
        'onChange' => 'this.form.submit();',
      ],
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Envoyer'),
      '#attributes' => [
        'style' => ['display: none;'],
      ],
    ];

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $termId = $form_state->getValue('theme');

    // Construire l'URL de redirection
    $url = \Drupal\Core\Url::fromUri('internal:/article-education-scolaire', [
      'query' => ['field_theme_target_id' => $termId]
    ]);

    // Rediriger vers l'URL
    $form_state->setRedirectUrl($url);
  }

}
