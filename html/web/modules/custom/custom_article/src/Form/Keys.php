<?php

namespace Drupal\custom_article\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;


class Keys extends FormBase {
  /**
   * {@inheritdoc}
   */
  // add function getformid
  public function getFormId() {
    return 'custom_article_keys';
  }
  public function buildForm(array $form, FormStateInterface $form_state) {
    $request = \Drupal::request();
    $defaultValue = $request->query->get('keys');
    $form['keys'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Recherche par mots clefs'),
      '#default_value' => $defaultValue ?: '',
      '#attributes'    => [
        'onChange' => 'this.form.submit();',
        'placeholder' => 'Mots clefs ...',
       // 'class' => 'form-item form-item-keys form-item--wrapper',

      ],
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Envoyer'),
      '#attributes' => [
        'style' => ['display: none;'],
      ],
    ];

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $termId = $form_state->getValue('keys');

    // Construire l'URL de redirection
    $url = \Drupal\Core\Url::fromUri('internal:/article-education-scolaire', [
      'query' => ['keys' => $termId]
    ]);

    // Rediriger vers l'URL
    $form_state->setRedirectUrl($url);
  }

}
