<?php

namespace Drupal\custom_article\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;


class NosDossiersForm extends FormBase {
  /**
   * {@inheritdoc}
   */
  // add function getformid
  public function getFormId() {
    return 'custom_article_nos_dossiers_form';
  }
  public function buildForm(array $form, FormStateInterface $form_state) {
   $term_ids = \Drupal::entityQuery('taxonomy_term')
    ->condition('vid', 'actualites')
    ->condition('status', 1) // Ajoutez cette ligne pour filtrer par les termes publiés uniquement
    ->execute();

$terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadMultiple($term_ids);

    $options = [];
    foreach ($terms as $term) {
      $options[$term->tid] = $term->name;
    }
    // ajouter un tire au formulaire

    $form['title'] = [
      '#markup' => '<h2>Nos dossiers</h2>',
    ];

    $form['actualites'] = [
      '#type' => 'select',
      //'#title' => $this->t(''),
      '#options' => $options,
      '#multiple' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Envoyer'),

    ];

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $termId = $form_state->getValue('actualites');

    // Construire l'URL de redirection
    $url = \Drupal\Core\Url::fromUri('internal:/article-education-scolaire', [
      'query' => ['field_actualites_target_id[]' => reset($termId)]
    ]);

    // Rediriger vers l'URL
    $form_state->setRedirectUrl($url);
  }

}
