<?php

namespace Drupal\custom_article\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;


class NosDossiersarticle extends FormBase {
  /**
   * {@inheritdoc}
   */
  // add function getformid
  public function getFormId() {
    return 'custom_article_nos_dossiers_form_article';
  }
  public function buildForm(array $form, FormStateInterface $form_state) {
  $terms =\Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('actualites');

    $options = [];
  
    foreach ($terms as $term) {
if($term->status ==1){
      $options[$term->tid] = $term->name;}
    }
    // ajouter un tire au formulaire

    $pattern = '/field_actualites_target_id%5B%5D=(.*?)(&|$)/';
    $url = \Drupal::request()->getRequestUri();;

    // Perform the regular expression match
    if (preg_match($pattern, $url, $matches)) {
      // Extracted value is in $matches[1]
      $extractedValue = $matches[1];

    } else {
      $extractedValue = '';

    }
    $form['actualites'] = [
      '#type' => 'select',
      '#title' => $this->t('Nos dossiers d’actualité'),
      '#options' => $options,
      '#multiple' => TRUE,
      '#default_value' => $extractedValue,
      '#attributes'    => [
        'onChange' => 'this.form.submit();',
      ],
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Envoyer'),
      '#attributes' => [
        'style' => ['display: none;'],
      ],
    ];

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $termId = $form_state->getValue('actualites');

    // Construire l'URL de redirection
    $url = \Drupal\Core\Url::fromUri('internal:/article-education-scolaire', [
      'query' => ['field_actualites_target_id[]' => reset($termId)]
    ]);

    // Rediriger vers l'URL
    $form_state->setRedirectUrl($url);
  }

}
