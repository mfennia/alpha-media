<?php

namespace Drupal\custom_article\Plugin\EmbeddedContent;

use Drupal\ckeditor5_embedded_content\EmbeddedContentInterface;
use Drupal\ckeditor5_embedded_content\EmbeddedContentPluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Plugin iframes.
 *
 * @EmbeddedContent(
 *   id = "Veille",
 *   label = @Translation("Veille"),
 *   description = @Translation("Veille"),
 * )
 */
class Veille extends EmbeddedContentPluginBase implements EmbeddedContentInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'mailto' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {



    return [
      '#theme' => 'ckeditor5_embedded_content_veille',

      '#etoile' => $this->configuration['etoile'],
      '#lienpresse' => $this->configuration['lienpresse'],
      '#titre' => $this->configuration['titre']['value'],
      '#titredate' => $this->configuration['titredate'],
      '#description' => $this->configuration['description']['value'],
      
      '#date' => $this->configuration['date'],
    ];
  }


  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    // add field type image


    //add field type texte area
    $form['etoile'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Nombre etoile'),
      //      '#format' => 'full_html',
      '#default_value' => $this->configuration['etoile'],
      '#required' => false,
    ];
    $form['lienpresse'] = [
      '#type' => 'textfield',
      '#title' => $this->t('lienpresse'),
      //      '#format' => 'full_html',
      '#default_value' => $this->configuration['lienpresse'],
      '#required' => false,
    ];
    $form['titre'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Titre'),
      '#format' => 'full_html',
      '#default_value' => $this->configuration['titre']['value'],
      '#required' => TRUE,
    ];
    $form['titredate'] = [
      '#type' => 'textfield',
      '#title' => $this->t('titre Gras '),

      '#default_value' => $this->configuration['titredate'],
      '#required' => TRUE,
    ];
    $form['date'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Date prés titre'),

      '#default_value' => $this->configuration['date'],
      '#required' => TRUE,
    ];
    $form['description'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Description'),
      '#format' => 'full_html',
      '#default_value' => $this->configuration['description']['value'],
      '#required' => TRUE,
    ];

 
    return $form;
  }

}
