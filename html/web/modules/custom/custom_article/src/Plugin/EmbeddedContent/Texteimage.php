<?php

namespace Drupal\custom_article\Plugin\EmbeddedContent;

use Drupal\ckeditor5_embedded_content\EmbeddedContentInterface;
use Drupal\ckeditor5_embedded_content\EmbeddedContentPluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\file\Entity\File;

/**
 * Plugin iframes.
 *
 * @EmbeddedContent(
 *   id = "texteimage",
 *   label = @Translation("Texte + image template "),
 *   description = @Translation("Texte + image template"),
 * )
 */
class Texteimage extends EmbeddedContentPluginBase implements EmbeddedContentInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'image' => NULL,
      'textarea' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $fid = reset($this->configuration['image']);//The file ID
    // get image uri from file id
//    $path = \Drupal::service('file_system')->realpath(\Drupal\file\Entity\File::load($fid)->getFileUri());
    $file = File::load($fid);
    if(!empty($file)){
      $filenames = $file->getFilename() ?? NULL;
    }else{
      $filenames = '';
    }


    return [
      '#theme' => 'ckeditor5_embedded_content_texteimage',
      '#image' => '/sites/default/files/imagesarticle/' . $filenames ,
      '#textarea' => $this->configuration['textarea']['value'],
    ];
  }


  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
// add field type image

    $form['image'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Image'),
      '#default_value' => $this->configuration['image'],
      '#upload_location' => 'public://imagesarticle/',
     '#required' => TRUE,
    ];
  //add field type texte area
    $form['textarea'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Description'),
      '#format' => 'full_html',
      '#default_value' => $this->configuration['textarea']['value'],
      '#required' => TRUE,
    ];
    return $form;
  }

}
