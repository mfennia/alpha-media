<?php

namespace Drupal\custom_article\Plugin\EmbeddedContent;

use Drupal\ckeditor5_embedded_content\EmbeddedContentInterface;
use Drupal\ckeditor5_embedded_content\EmbeddedContentPluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Plugin iframes.
 *
 * @EmbeddedContent(
 *   id = "mailto",
 *   label = @Translation("Mailto"),
 *   description = @Translation("Mailto"),
 * )
 */
class Mailto extends EmbeddedContentPluginBase implements EmbeddedContentInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'mailto' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {



    return [
      '#theme' => 'ckeditor5_embedded_content_mailto',

      '#textarea' => $this->configuration['lienarticle'],
    ];
  }


  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    // add field type image


    //add field type texte area
    $form['lienarticle'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Entrez  l'URL  pour l'article à partager par e-mail"),
      '#default_value' => $this->configuration['lienarticle'],
      '#required' => TRUE,
    ];
    return $form;
  }

}
