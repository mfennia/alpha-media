<?php

namespace Drupal\custom_article\Plugin\Field\FieldFormatter;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\node\Entity\Node;
use Drupal\Core\Database\Database;

/**
 * Plugin implementation of the 'my_custom_image_link' formatter.
 *
 * @FieldFormatter(
 *   id = "article_image_link",
 *   label = @Translation("Image Link"),
 *   field_types = {
 *     "image"
 *   }
 * )
 */
class MyCustomImageLinkFormatter extends ImageFormatterBase {

  /**
   * The image style entity storage.
   *
   * @var \Drupal\image\ImageStyleStorageInterface
   */
  protected $imageStyleStorage;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Constructs an ImageFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Entity\EntityStorageInterface $image_style_storage
   *   The image style storage.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, EntityStorageInterface $image_style_storage, AccountInterface $current_user) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->imageStyleStorage = $image_style_storage;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager')->getStorage('image_style'),
      $container->get('current_user'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'image_style' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);

    unset($element['image_link'], $element['image_loading']);

    $image_styles = image_style_options(FALSE);
    $description_link = Link::fromTextAndUrl(
      $this->t('Configure Image Styles'),
      Url::fromRoute('entity.image_style.collection')
    );
    $element['image_style'] = [
      '#title' => $this->t('Image style'),
      '#type' => 'select',
      '#default_value' => $this->getSetting('image_style'),
      '#empty_option' => $this->t('None (original image)'),
      '#options' => $image_styles,
      '#description' => $description_link->toRenderable() + [
          '#access' => $this->currentUser->hasPermission('administer image styles'),
        ],
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $image_styles = image_style_options(FALSE);
    // Unset possible 'No defined styles' option.
    unset($image_styles['']);
    // Styles could be lost because of enabled/disabled modules that defines
    // their styles in code.
    $image_style_setting = $this->getSetting('image_style');
    if (isset($image_styles[$image_style_setting])) {
      $summary[] = $this->t('Image style: @style', ['@style' => $image_styles[$image_style_setting]]);
    }
    else {
      $summary[] = $this->t('Original image');
    }

    return array_merge($summary, parent::settingsSummary());
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $items */
    if (empty($images = $this->getEntitiesToView($items, $langcode))) {
      // Early opt-out if the field is empty.
      return $elements;
    }

    /** @var \Drupal\image\ImageStyleInterface $image_style */
    $image_style = $this->imageStyleStorage->load('crop');
    /** @var \Drupal\Core\File\FileUrlGeneratorInterface $file_url_generator */
    $file_url_generator = \Drupal::service('file_url_generator');
    /** @var \Drupal\file\FileInterface[] $images */
    foreach ($images as $delta => $image) {
      $image_uri = $image->getFileUri();
      $url = $image_style ? $file_url_generator->transformRelative($image_style->buildUrl($image_uri)) : $file_url_generator->generateString($image_uri);

      // Add cacheability metadata from the image and image style.
      $cacheability = CacheableMetadata::createFromObject($image);
      if ($image_style) {
        $cacheability->addCacheableDependency(CacheableMetadata::createFromObject($image_style));
      }
      $existvideol = \Drupal\views\Views::getView('video_per_image');
      $existvideol->setDisplay('default');
      $existvideol->setArguments([$image->id()]);
      $existvideol->execute();
      $existde = \Drupal\views\Views::getView('descrition_per_image');
      $existde->setDisplay('default');
      $existde->setArguments([$image->id()]);
      $existde->execute();
      if(isset(reset($existvideol->result)->_entity->field_video)){
        if (!(reset($existvideol->result)->_entity->field_video->isEmpty())) {
          $url1 = \Drupal\Core\Url::fromUri('internal:/video-per-image/' . $image->id());

          $elements[$delta] = ['#markup' => '<a href =' . $url1->toString() . ' class="use-ajax" data-dialog-type="modal" data-dialog-options="{&quot;width&quot;:800}" ><img src="' . $url . '" /></a>'];

        }
        else {
          if (!(reset($existde->result)->_entity->field_description_pp->isEmpty())) {
            $url1 = \Drupal\Core\Url::fromUri('internal:/des-per-image/' . $image->id());

            $elements[$delta] = ['#markup' => '<div class="descpp"><a href =' . $url1->toString() . ' class="use-ajax descpp" data-dialog-type="modal" data-dialog-options="{&quot;width&quot;:800}" ><img src="' . $url . '" /></a></div>'];

          }else{

            $node = \Drupal::routeMatch()->getParameter('node');
            if(isset($node)){
              $urlz = \Drupal\Core\Url::fromUri('internal:/description/'.$node->id() );
            }else{
              $urlz = \Drupal\Core\Url::fromUri('internal:/description/'.$this->get_last_modified_homepage_article_id() );
            }
            

            $elements[$delta] = ['#markup' => '<div class="descpp"><a href =' . $urlz->toString() . ' class="use-ajax descpp" data-dialog-type="modal" data-dialog-options="{&quot;width&quot;:800}" ><img src="' . $url . '" /></a></div>'];
          }


        }
        $cacheability->applyTo($elements[$delta]);
      }


    }
    return $elements;
  }
  function get_last_modified_homepage_article_id() {
    $query = \Drupal::entityQuery('node')
      ->condition('status', 1) // Assure que l'article est publié.
      ->condition('type', 'article') // Remplacez 'article' par le type de contenu approprié.
      ->condition('field_mise_en_page_d_accueil', 1) // 1 pour TRUE (coché).
 ->accessCheck('TRUE')
      ->sort('changed', 'DESC') // Trié par date de modification décroissante.
      ->range(0, 1); // Limite à un seul résultat.

    $nids = $query->execute();
    if (!empty($nids)) {
      $nid = reset($nids); // Prend le premier et seul élément du tableau.
      return $nid;
    }

    return NULL; // Retourne NULL si aucun nœud n'est trouvé.
  }

}
