<?php

namespace Drupal\custom_article\Plugin\Field\FieldFormatter;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;

/**
 * Plugin implementation of the 'entity reference rendered entity' formatter.
 *
 * @FieldFormatter(
 *   id = "entity_reference_entity_view_first",
 *   label = @Translation("Rendered entity first"),
 *   description = @Translation("Rendered entity first"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class EntityReferenceEntityFormatter extends \Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceEntityFormatter {
  protected function getEntitiesToView(EntityReferenceFieldItemListInterface $items, $langcode) {
    $entities = parent::getEntitiesToView($items, $langcode);
    if(!empty($entities)){
      $entities  = [0 => $entities[0]];
    }
    return $entities;
  }
}
