<?php

namespace Drupal\custom_article\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Render\Markup;
use Drupal\taxonomy\Entity\Term;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Provides a 'Custom Next Article Block' block.
 *
 * @Block(
 *   id = "custom_description_article_block",
 *   admin_label = @Translation("Custom description Article Block"),
 * )
 */
class CustomArticleDescription extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The route match service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RouteMatchInterface $route_match) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Get the current language.
    $current_language = \Drupal::languageManager()->getCurrentLanguage();

    // Load the taxonomy terms from the 'taxo' vocabulary.
    $terms = \Drupal::entityTypeManager()
      ->getStorage('taxonomy_term')
      ->loadTree('analyse', 0, 1, TRUE);

    // Initialize an array to store term names in the current language.
    $term_names = [];

    foreach ($terms as $term) {
      // Load the term entity.

      // Get the term name in the current language.
      $term_name_in_current_language = $term->getTranslation($current_language->getId())->getName();

      // Add the term name to the array.
      $term_names[] = $term_name_in_current_language;
    }

    // Create a formatted string with the term names.
    $term_names_string = implode(', ', $term_names);

    $term_names_with_span = Markup::create('<span class="termsnames">' . $term_names_string . '</span>');
    // Translate the string using Drupal 9's translation functions.
    $translated_string = \Drupal::translation()
      ->translate("Toute l'actualité de  l'éducation : @term_names ..." , ['@term_names' => $term_names_with_span]);

    return [
      '#markup' => '<div class="terms">'.$translated_string.'</div>',
      '#cache' => [
        'max-age' => 0,
      ],
    ];

  }

}
