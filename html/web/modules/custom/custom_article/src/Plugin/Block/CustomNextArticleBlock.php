<?php

namespace Drupal\custom_article\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Provides a 'Custom Next Article Block' block.
 *
 * @Block(
 *   id = "custom_next_article_block",
 *   admin_label = @Translation("Custom Next Article Block"),
 * )
 */
class CustomNextArticleBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The route match service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RouteMatchInterface $route_match) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Get the current node.
    $node = $this->routeMatch->getParameter('node');

    // Initialize the next node variable.
    $next_node = NULL;

    // Check if the current page is a node of type "article."
    if ($node instanceof Node && $node->getType() == 'article') {
      // Load nodes of type "article" created after the current node.
      $query = \Drupal::entityQuery('node')
        ->accessCheck(TRUE)
        ->condition('type', 'article');
//        ->condition('created', $node->getCreatedTime(), '>')
//        ->sort('created');
      $result = $query->execute();

      // Get the next node (if it exists).
      if (!empty($result)) {
        $next_node_id = reset($result);
        $next_node = Node::load($next_node_id);
      }

    }

    // Build the block content.
    if ($next_node) {

      return [
        '#markup' => '<a class="btn-flesh" href="/node/'.$next_node->id().'"  />Next</a>',
      ];
    }
    else {
      return [
        '#markup' => '<a class="btn-flesh" href="/node/'.$next_node->id().'"  />Next</a>',
      ];
    }
  }

}
