<?php

namespace Drupal\custom_article\Plugin\views\area;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy\Entity\Term;
use Drupal\views\Plugin\views\area\AreaPluginBase;
use Drupal\views\Plugin\views\style\DefaultSummary;

/**
 * Views area handler to display some configurable result summary.
 *
 * @ingroup views_area_handlers
 *
 * @ViewsArea("customarea")
 */
class CustomArea extends AreaPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['content'] = [
      'default' => $this->t('@total articles '),
    ];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $item_list = [
      '#theme' => 'item_list',
      '#items' => [
        '@start -- the initial record number in the set',
        '@date -- date of the article',
        '@theme -- theme of the article',
        '@actus -- actus of the article',
        '@inter -- intervenant of the article',
        '@descriptionactus -- descriptionactus of the article',
        '@keys -- keys of the article',
        '@end -- the last record number in the set',
        '@total -- the total records in the set',
        '@label -- the human-readable name of the view',
        '@per_page -- the number of items per page',
        '@current_page -- the current page number',
        '@current_record_count -- the current page record count',
        '@page_count -- the total page count',
      ],
    ];
    $list = \Drupal::service('renderer')->render($item_list);
    $form['content'] = [
      '#title' => $this->t('Display'),
      '#type' => 'textarea',
      '#rows' => 3,
      '#default_value' => $this->options['content'],
      '#description' => $this->t('You may use HTML code in this field. The following tokens are supported:') . $list,
    ];

  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    if (str_contains($this->options['content'], '@total')) {
      $this->view->get_total_rows = TRUE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function render($empty = FALSE) {
    // Must have options and does not work on summaries.
    if (!isset($this->options['content']) || $this->view->style_plugin instanceof DefaultSummary) {
      return [];
    }
    $output = '';
    $format = $this->options['content'];
    // Calculate the page totals.
    $current_page = (int) $this->view->getCurrentPage() + 1;
    $per_page = (int) $this->view->getItemsPerPage();
    // @TODO: Maybe use a possible is views empty functionality.
    // Not every view has total_rows set, use view->result instead.
    $total = $this->view->total_rows ?? count($this->view->result);
    $label = Html::escape($this->view->storage->label());
    // If there is no result the "start" and "current_record_count" should be
    // equal to 0. To have the same calculation logic, we use a "start offset"
    // to handle all the cases.
    $start_offset = empty($total) ? 0 : 1;
    if ($per_page === 0) {
      $page_count = 1;
      $start = $start_offset;
      $end = $total;
    }
    else {
      $page_count = (int) ceil($total / $per_page);
      $total_count = $current_page * $per_page;
      if ($total_count > $total) {
        $total_count = $total;
      }
      $start = ($current_page - 1) * $per_page + $start_offset;
      $end = $total_count;
    }

    $current_record_count = ($end - $start) + $start_offset;
    // Get the search information.
    $replacements = [];
    $date = \Drupal::request()->query->get('field_date_de_publlication_value');
    if(!empty($date)){
      $txtdate = 'en ' . $date;
      $replacements['@date'] = $txtdate;
    }else{
      $replacements['@date'] = '';
    }

    $theme = \Drupal::request()->query->get('field_theme_target_id');
    if(!empty($theme) && $theme != 'All'){
      $term = Term::load($theme);
      $txttheme = '"' .$term->getName().'"';
if($total == 0 ){
$replacements['@theme'] = "Aucun article encore sur ce thème.";
}
else{
 $replacements['@theme'] = "Tous nos articles sur " . $txttheme;
}

     
    }else{
      $replacements['@theme'] = '';
    }

    $inter = \Drupal::request()->query->get('field_intervenants_target_id');
    if(!empty($inter) && $inter != 'All'){

      $inter = str_replace("+", " ", $inter);

if($total == 0 ){
$replacements['@inter'] = "Aucun article encore sur cet intervenant.";
}
else{
$replacements['@inter'] = "Tous nos articles avec " . '"' .$inter.'"';
}
      
    }else{
      $replacements['@inter'] = '';
    }

    //    $actus = \Drupal::request()->query->get('field_actualites_target_id');
    $pattern = '/field_actualites_target_id%5B%5D=(.*?)(&|$)/';
    $url = \Drupal::request()->getRequestUri();;

    // Perform the regular expression match
    if (preg_match($pattern, $url, $matches)) {
      // Extracted value is in $matches[1]
      $extractedValue = $matches[1];

    } else {
      // Pattern not found

    }
    $keys = \Drupal::request()->query->get('keys');
    if(!empty($keys) && $keys != 'All'){
      $txtkeys = 'Tous nos articles sur ' . $keys;
      $replacements['@keys'] = $txtkeys;
    }else{
      $replacements['@keys'] = '';
    }
    if(!empty($extractedValue) && $extractedValue != 'All'){
      $termactus = Term::load($extractedValue);
      //      dump($termactus->description->value);
      $txtactus = '"' .$termactus->getName().'"';
      $descriptionactus = $termactus->description->value;
      $replacements['@actus'] = "Notre dossier sur " . $txtactus;
      $replacements['@descriptionactus'] = '<br> <br><div class="descriptionactus">'.$descriptionactus.'</div>';
    }else{
      $replacements['@actus'] = '';
      $replacements['@descriptionactus'] = '';
    }
    //    $replacements['@actus'] = '';
    $replacements['@start'] = $start;
    $replacements['@end'] = $end;
    $replacements['@total'] = $total;
    $replacements['@label'] = $label;
    $replacements['@per_page'] = $per_page;
    $replacements['@current_page'] = $current_page;
    $replacements['@current_record_count'] = $current_record_count;
    $replacements['@page_count'] = $page_count;
    // Send the output.
    if (!empty($total) || !empty($this->options['empty'])) {
      $output .= str_replace(array_keys($replacements), array_values($replacements), $format);
      // Return as render array.
      return [
        '#markup' => $output,
      ];
    }

    return [];
  }

}
