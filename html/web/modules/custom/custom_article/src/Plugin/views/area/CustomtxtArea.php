<?php


namespace Drupal\custom_article\Plugin\views\area;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\area\TokenizeAreaPluginBase;

/**
 * Views area text handler.
 *
 * @ingroup views_area_handlers
 *
 * @ViewsArea("text_custom_custom")
 */
class CustomtxtArea extends TokenizeAreaPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['content'] = [
      'contains' => [
        'value' => ['default' => ''],
        'format' => ['default' => NULL],
      ],
    ];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $html ="<a href='/article-education-scolaire'>Tous nos articles</a>
<div class='tn-group dropend nosDossiers'>
    <button type='button' class='btn btn-secondary dropdown-toggle dropdown-toggle-split nosDossiersLink1' data-bs-toggle='dropdown' aria-expanded='false'>
      <span>Nos dossiers</span>
    </button>
    <ul class='dropdown-menu' style=''>
<li><a class='dropdown-item nosDossiersLink2' href='/article-education-scolaire?field_actualites_target_id%5B%5D=51'>Parcoursup</a></li>
<li><a class='dropdown-item nosDossiersLink2' href='/article-education-scolaire?field_actualites_target_id%5B%5D=8'>Les rythmes scolaires</a></li>
<li><a class='dropdown-item nosDossiersLink2' href='/article-education-scolaire?field_actualites_target_id%5B%5D=9'>Les troubles 'Dys</a></li>
    </ul>
  </div>
<a href='/node/32'>Revue de presse</a>
<a href='/node/46'>Veille institutionnelle</a>";
    $form['content'] = [
      '#title' => $this->t('Content'),
      '#type' => 'text_format',
      '#default_value' => $html,
      '#rows' => 6,
      '#format' => $this->options['content']['format'] ?? filter_default_format(),
      '#editor' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function preQuery() {
    $content = $this->options['content']['value'];
    // Check for tokens that require a total row count.
    if (str_contains($content, '[view:page-count]') || str_contains($content, '[view:total-rows]')) {
      $this->view->get_total_rows = TRUE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function render($empty = FALSE) {
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('actualites');
    $html_output = ''; // Initialisation de la variable pour stocker le HTML

    foreach ($terms as $term) {
      // Charger l'entité terme complète pour accéder à son statut.
      $term_entity = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($term->tid);

      if (!empty($term_entity) && $term_entity->isPublished()) {
        // Concaténer le code HTML pour chaque terme publié à la variable $html_output.
        $html_output .= "<li><a class='dropdown-item nosDossiersLink2' href='/article-education-scolaire?field_actualites_target_id%5B%5D=" . $term->tid . "'>" . $term->name . "</a></li>";
      }
    }

    // À ce stade, $html_output contient tout le HTML généré. Vous pouvez l'utiliser comme vous le souhaitez.


    $html ="<a href='/article-education-scolaire'>Tous nos articles</a>
<div class='tn-group dropend nosDossiers'>
    <button type='button' class='btn btn-secondary dropdown-toggle dropdown-toggle-split nosDossiersLink1' data-bs-toggle='dropdown' aria-expanded='false'>
      <span>Nos dossiers</span>
    </button>
    <ul class='dropdown-menu' style=''>
'.$html_output.'
    </ul>
  </div>
<a href='/node/32'>Revue de presse</a>
<a href='/node/46'>Veille institutionnelle</a>";
    $format = $this->options['content']['format'] ?? filter_default_format();
    if (!$empty || !empty($this->options['empty'])) {
      return [
        '#type' => 'processed_text',
        '#text' => $this->tokenizeValue($html),
        '#format' => $format,
      ];
    }

    return [];
  }

}
