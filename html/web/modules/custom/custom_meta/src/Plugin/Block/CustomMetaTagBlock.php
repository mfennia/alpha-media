<?php


namespace Drupal\custom_meta\Plugin\Block;

use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;


/**
 * Provides A meta tags for a current page.
 *
 * @Block(
 *   id = "block_custom_meta_tag",
 *   admin_label = @Translation("Block Custom Meta tag"),
 *   category = @Translation("SEO")
 * )
 */
class CustomMetaTagBlock extends BlockBase {

  /**
   * Builds and returns the renderable array for this block plugin.
   *
   * If a block should not be rendered because it has no content, then this
   * method must also ensure to return no content: it must then only return an
   * empty array, or an empty array with #cache set (with cacheability metadata
   * indicating the circumstances for it being empty).
   *
   * @return array
   *   A renderable array representing the content of the block.
   *
   * @see \Drupal\block\BlockViewBuilder
   */
  public function build() {
    //Recuperate the config form from the form existed in Directory Form.
    $build = \Drupal::formBuilder()
      ->getForm('\Drupal\custom_meta\Form\CustomMetaTagForm');
    return $build;
  }
}