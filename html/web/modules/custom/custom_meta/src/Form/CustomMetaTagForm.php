<?php


namespace Drupal\custom_meta\Form;

use Drupal\Core\Ajax\AfterCommand;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class CustomMetaTagForm
 * @package Drupal\custom_meta\Form
 */
class CustomMetaTagForm extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    //Get all configurations.
    $configuration = $this->configFactory()
      ->getEditable('custom_meta.settings')
      ->getRawData();
    $tmp_path = \Drupal::service('path.current')->getPath();
    $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $current_path = $language . $tmp_path;
    $moved_path = str_replace('/', "", $current_path);
    $request = \Drupal::request();
    $route_match = \Drupal::routeMatch();
    if (isset($configuration[$moved_path]['meta_title'])) {
      if ($configuration[$moved_path]['meta_title'] == "") {
        if (\Drupal::service('path.matcher')->isFrontPage()) {
          $title_aux_ = \Drupal::config('system.site')->get('name');
          $title_aux = mb_convert_encoding($title_aux_, 'UTF-8');

        }else{
          $title_aux_ = \Drupal::service('title_resolver')
            ->getTitle($request, $route_match->getRouteObject());
          $title_aux = mb_convert_encoding($title_aux_, 'UTF-8');

        }
      }
      else {
        $title_aux_ = $configuration[$moved_path]['meta_title'];
        $title_aux = mb_convert_encoding($title_aux_, 'UTF-8');

      }

    }else{
      if (\Drupal::service('path.matcher')->isFrontPage()) {
        $title_aux_ = \Drupal::config('system.site')->get('name');
        $title_aux = mb_convert_encoding($title_aux_, 'UTF-8');

      }else{
        $title_aux_ = \Drupal::service('title_resolver')
          ->getTitle($request, $route_match->getRouteObject());
        $title_aux = mb_convert_encoding($title_aux_, 'UTF-8');
      }
    }
    $form['current_path'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Current Path'),
      '#required' => TRUE,
      '#disabled' => TRUE,
      '#default_value' => $current_path,
    );
    $form['meta_title'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Meta Title'),
/*      '#required' => TRUE,*/
      '#default_value' =>  $title_aux,
    );
    $form['meta_description'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('Meta Description'),
/*      '#required' => TRUE,*/
      '#attributes' => array('maxlength' => 160),
      '#rows'=>3,
      '#cols'=> 30,
      '#default_value' => isset($configuration[$moved_path]['meta_description']) ? $configuration[$moved_path]['meta_description'] : "",
    );
    $form['meta_keywords'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Meta Keywords'),
/*      '#required' => TRUE,*/
      '#default_value' => isset($configuration[$moved_path]['meta_keywords']) ? $configuration[$moved_path]['meta_keywords'] : "",
    );
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Save configuration'),
      '#button_type' => 'primary',
    );
    // By default, render the form using theme_system_config_form().
    $form['#theme'] = 'system_config_form';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $tmp_path = \Drupal::service('path.current')->getPath();

    $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $current_path = $language . $tmp_path;
    //Correct the path( delete all slashes).
    $moved_path = str_replace('/', "", $current_path);

      //Save configurations.
      $current_config = array(
        'meta_title' => $form_state->getValue('meta_title'),
        'meta_description' => $form_state->getValue('meta_description'),
        'meta_keywords' => $form_state->getValue('meta_keywords')
      );

      $this->configFactory()
        ->getEditable('custom_meta.settings')
        ->set($moved_path, $current_config)
        ->save(TRUE);

    //Drupal::messenger()->addMessage($this->t('The configuration options have been saved.'));
  }


  /**
   * Gets the configuration names that will be editable.
   *
   * @return array
   *   An array of configuration object names that are editable if called in
   *   conjunction with the trait's config() method.
   */
  protected function getEditableConfigNames() {
    return ['custom_meta.settings'];
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'custom_meta_tags_form_id';
  }
}
