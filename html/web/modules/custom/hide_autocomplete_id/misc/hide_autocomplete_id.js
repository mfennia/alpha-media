(function ($, Drupal) {

  'use strict';

  let beforeSerialize = Drupal.Ajax.prototype.beforeSerialize;
  Drupal.Ajax.prototype.beforeSerialize = function (element, options) {
    if (this.$form) {
      this.$form.find('.form-autocomplete').each(function(){
        let realData = $(this).data('real-value');
        let value = $(this).val();
        if (value && realData) {
          let checkRealData = Drupal.autocomplete.splitValues(realData);
          let checkValue = Drupal.autocomplete.splitValues(value);
          for (let i in checkValue) {
            let entityIdMatch = checkRealData[i].match(/\s*\((\d*)\)$/);
            if (entityIdMatch) {
              let check = checkRealData[i].replace(entityIdMatch[0], '');
              if (check != checkValue[i]) {
                delete(checkRealData[i]);
              }
            }
          }
          $(this).val(checkRealData.join(','));
        }
      });
    }
    beforeSerialize.call(this, element, options);
  };

  /**
   * Remove entity reference ID from "entity_autocomplete" field.
   *
   * @type {{attach: Drupal.behaviors.autocompleteReferenceEntityId.attach}}
   */
  Drupal.behaviors.autocompleteReferenceEntityId = {
    attach: function (context) {
      $(once('replaceReferenceIdOnInit', 'form')).each(function(){
      //$('form').once('replaceReferenceIdOnInit').each(function(){
        $(this).on('submit', function(){
          $(this).find('.form-autocomplete').each(function(){
            let realData = $(this).data('real-value');
            let value = $(this).val();
            if (value && realData) {
              let checkRealData = Drupal.autocomplete.splitValues(realData);
              let checkValue = Drupal.autocomplete.splitValues(value);
              for (let i in checkValue) {
                let entityIdMatch = checkRealData[i].match(/\s*\((\d*)\)$/);
                if (entityIdMatch) {
                  let check = checkRealData[i].replace(entityIdMatch[0], '');
                  if (check != checkValue[i]) {
                    delete(checkRealData[i]);
                  }
                }
              }
              $(this).val(checkRealData.join(','));
            }
          });
        })
      });
      // Remove reference IDs for autocomplete elements on init.
      
      $(once('replaceReferenceIdOnInit', '.form-autocomplete', context)).each(function(){
      //$('.form-autocomplete', context).once('replaceReferenceIdOnInit').each(function () {
        let splitValues = (this.value && this.value !== 'false') ?
            Drupal.autocomplete.splitValues(this.value) : [];
        if (splitValues.length > 0) {
          let labelValues = [];
          for (let i in splitValues) {
            let value = splitValues[i].trim();
            let entityIdMatch = value.match(/\s*\((\d*)\)$/);
            if (entityIdMatch) {
              labelValues[i] = value.replace(entityIdMatch[0], '');
            }
          }
          if (labelValues.length > 0) {
            $(this).data('real-value', splitValues.join(', '));
            this.value = labelValues.join(', ');
          }
        }
      });
    }
  };

  let autocomplete = Drupal.autocomplete.options;
  autocomplete.originalValues = [];
  autocomplete.labelValues = [];

  /**
   * Add custom select handler.
   */
  autocomplete.select = function (event, ui) {
    autocomplete.labelValues = Drupal.autocomplete.splitValues(event.target.value);
    autocomplete.labelValues.pop();
    autocomplete.labelValues.push(ui.item.label);
    autocomplete.originalValues.push(ui.item.value);

    $(event.target).data('real-value', autocomplete.originalValues.join(', '));
    event.target.value = autocomplete.labelValues.join(', ');

    return false;
  }

})(jQuery, Drupal);
