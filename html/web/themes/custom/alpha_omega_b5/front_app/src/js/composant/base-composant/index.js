(function ($) {
  "use strict";
  function Menu() {
    const $menu = $("header .menu--main > ul");
    const $menuLi = $menu.find("li.dropdown > a");
    $menuLi.click(function () {
      $(this).parent().find("> .dropdown-menu").slideToggle("slow");
      return false;
    });
  }
  const isMobile = window.matchMedia("(max-width: 991px)").matches;
  if (isMobile) {
    Menu();
  }
  $(window).resize(function () {
    if (isMobile) {
      Menu();
    }
  });
  setTimeout(function () {
    $("#views-exposed-form-article-list-page-1 .form-item").each(function () {
      $(this)
        .find("select,input")
        .wrapAll("<div class='form-item--wrapper'></div>");
    });
    $(
      "#views-exposed-form-article-list-page-1 .form-item-keys .form-text"
    ).each(function (i, el) {
      if (!el.value || el.value == "") {
        el.placeholder = "Mots clefs ...";
      }
    });
    autocomplete();
  }, 1000);

  function autocomplete() {
    console.log("autocomplete");
    // Assuming the autocomplete input has an ID of 'edit-field-intervenants-target-id--2'
    $("#edit-intervenant").on("autocompleteclose", function () {
      // Submit the form when an autocomplete option is selected
      $(this).closest("form").submit();
    });
  }

  // var swiper = new Swiper(".view-article-teaser", {
  //   slidesPerView: 4,
  //   spaceBetween: 25,
  //   navigation: {
  //     nextEl: ".view-article-connexes .swiper-button-next",
  //     prevEl: ".view-article-connexes .swiper-button-prev",
  //   },
  //   breakpoints: {
  //     "@0.00": {
  //       slidesPerView: 2,
  //       spaceBetween: 16,
  //     },
  //     "@0.75": {
  //       slidesPerView: 2,
  //       spaceBetween: 16,
  //     },
  //     "@1.00": {
  //       slidesPerView: 4,
  //       spaceBetween: 16,
  //     },
  //     "@1.50": {
  //       slidesPerView: 4,
  //       spaceBetween: 25,
  //     },
  //   },
  // });

  var swiper = new Swiper(".view-article-teaser", {
    slidesPerView: 4,
    spaceBetween: 25,
    navigation: {
      nextEl: ".view-article-connexes .swiper-button-next",
      prevEl: ".view-article-connexes .swiper-button-prev",
    },
    breakpoints: {
      "@0.00": {
        slidesPerView: 2,
        spaceBetween: 16,
      },
      "@0.75": {
        slidesPerView: 2,
        spaceBetween: 16,
      },
      "@1.00": {
        slidesPerView: 4,
        spaceBetween: 16,
      },
      "@1.50": {
        slidesPerView: 4,
        spaceBetween: 25,
      },
    },
    on: {
      reachEnd: function () {
        // Attendre un peu avant de revenir au début
        setTimeout(() => {
          this.slideTo(0, 2000); // Revenir à la première diapositive
        }, 1000); // Ajustez le délai selon vos besoins
      },
    },
  });

  // var swiperassociation = new Swiper(".view-association", {
  //   slidesPerView: 4,
  //   spaceBetween: 25,
  //   navigation: {
  //     nextEl: ".view-association .swiper-button-next",
  //     prevEl: ".view-association .swiper-button-prev",
  //   },
  //   breakpoints: {
  //     "@0.00": {
  //       slidesPerView: 2,
  //       spaceBetween: 16,
  //     },
  //     "@0.75": {
  //       slidesPerView: 2,
  //       spaceBetween: 16,
  //     },
  //     "@1.00": {
  //       slidesPerView: 4,
  //       spaceBetween: 16,
  //     },
  //     "@1.50": {
  //       slidesPerView: 4,
  //       spaceBetween: 25,
  //     },
  //   },
  // });

  var swiperFirst = new Swiper(".temoignage-slider", {
    slidesPerView: 1,
    loop: true,
    autoplay: {
      delay: 3000,
    },
    pagination: {
      el: ".swiper-pagination",
      clickable: true,
    },
  });

  Drupal.behaviors.myBehavior = {
    attach: function (context, settings) {
      const player = Plyr.setup("video", {});
    },
  };

  var swiperpage = new Swiper(".swiper-page", {
    slidesPerView: 4,
    spaceBetween: 15,
    updateOnWindowResize: true,
    loop: false,
    grabCursor: true,
    centeredSlides: false,
    centeredSlidesBounds: false,
    initialSlide: 0,
    navigation: {
      nextEl: ".swiper-swiper-page .swiper-button-next",
      prevEl: ".swiper-swiper-page .swiper-button-prev",
    },
    breakpoints: {
      "@0.00": {
        slidesPerView: 1,
        spaceBetween: 15,
      },
      "@0.75": {
        slidesPerView: 2,
        spaceBetween: 15,
      },
      "@1.00": {
        slidesPerView: 3,
        spaceBetween: 15,
      },
      "@1.50": {
        slidesPerView: 7,
        spaceBetween: 15,
      },
    },
    on: {
      click(event) {
        console.log(this.clickedIndex);
        swiperpage.slideTo(this.clickedIndex);
        $(".paragraph-item").hide();
        $(".paragraph-" + this.clickedIndex).show();
      },
    },
  });
  swiperpage.on("slideChange", function (e) {
    $(".paragraph-item").hide();
    $(".paragraph-" + this.activeIndex).show();
  });
})(jQuery);
